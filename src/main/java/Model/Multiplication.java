package Model;

import java.util.List;

public class Multiplication {

    public Polynomial multiply(Polynomial poly1, Polynomial poly2){
        List<Monomial> polinom1 = poly1.getPolynomial();
        List<Monomial> polinom2 = poly2.getPolynomial();
        Polynomial result = new Polynomial();
        int i=0, j;
        while( i<polinom1.size()){ j=0;
            while(j<polinom2.size()){
                Monomial term = new Monomial(0,0);
                float coeff = polinom1.get(i).getCoefficient()*polinom2.get(j).getCoefficient();
                int degree = polinom1.get(i).getDegree()+polinom2.get(j).getDegree();
                term.setDegree(degree);
                term.setCoefficient(coeff);
                result.insert(term);
                j++;
            }i++;
        }List<Monomial> result1 = result.getPolynomial();
        i=0;
        while (i<result1.size()-1)
        { j=i+1;
            while(j<result1.size()){
                if(result1.get(i).getDegree() == result1.get(j).getDegree())
                {
                result1.get(i).setCoefficient(result1.get(i).getCoefficient() + result1.get(j).getCoefficient());
                Monomial monom = result1.get(j);
                result.removeMonom(monom);
                }j++;
            }i++;
        }List<Monomial> resultDesc = result.getPolynomial();
       result.sortDesc(resultDesc);
       return result;
    }
}
