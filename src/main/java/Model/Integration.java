package Model;

import java.util.List;

public class Integration {

    public Polynomial integrate(Polynomial poly){

        List<Monomial> poly1 = poly.getPolynomial();
        Polynomial polyIntegrated = new Polynomial();
        int i=0;

        while(i<poly1.size()){
            Monomial term = new Monomial(0,0);

            float coeff = 1*poly1.get(i).getCoefficient()/(poly1.get(i).getDegree()+1);
            int degree = poly1.get(i).getDegree()+1;

            term.setCoefficient(coeff);
            term.setDegree(degree);

            polyIntegrated.insert(term);
            i++;
        }
        return polyIntegrated;
    }
}
