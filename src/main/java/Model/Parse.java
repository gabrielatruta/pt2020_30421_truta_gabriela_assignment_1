package Model;
import javax.swing.*;

public class Parse {
    public boolean parsePolynomial (String polyString, Polynomial poly) {
        if(polyString.equals(""))
        {
            JOptionPane.showMessageDialog(new JFrame(), "You need to enter the polynomials", "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        int i = 0, coeff = 0, degree = 0, j = 0, sign = 1;
        while (i < polyString.length()) {
            if(polyString.charAt(i) != '+' && polyString.charAt(i) != '-' && !(polyString.charAt(i) >= '0' && polyString.charAt(i) <= '9') && polyString.charAt(i) != 'x' && polyString.charAt(i) != '^')
            {
                JOptionPane.showMessageDialog(new JFrame(), "Unknown characters used", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }
            if (j == 1){
                if (polyString.charAt(i) >= '0' && polyString.charAt(i) <= '9'){
                    degree = degree * 10 + (polyString.charAt(i) - '0');
                    i++;
                }else j++;
                continue; }
            if (j == 0) {
                if (polyString.charAt(i) >= '0' && polyString.charAt(i) <= '9') {
                    coeff = coeff * 10 + (polyString.charAt(i) - '0');
                    i++;
                } else { if (polyString.charAt(i) == '-')
                    sign = -1;
                else { i++;
                    if(i < polyString.length() && polyString.charAt(i) == '^'){
                        i++;
                    }else j=2;
                    j++; }
                }continue; }

            if (coeff == 0) coeff = 1;
            if(polyString.charAt(i - 1) == 'x') degree = 1;
            Monomial term = new Monomial(coeff * sign, degree);
            poly.insert(term);
            coeff = 0; degree = 0 ; j = 0;
            if (polyString.charAt(i) == '-')
                sign = -1;
            else sign = 1;
            i++; }
        if (coeff == 0) coeff = 1;
        if(polyString.charAt(i - 1) == 'x') degree = 1;
        Monomial term = new Monomial(coeff * sign, degree);
        poly.insert(term);
        return true; }
}
