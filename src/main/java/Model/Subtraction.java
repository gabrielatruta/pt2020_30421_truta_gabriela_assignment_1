package Model;

import java.util.List;

public class Subtraction {

    public Polynomial subtractionPoly(Polynomial poly1, Polynomial poly2) {
        List<Monomial> polynom1 = poly1.getPolynomial(), polynom2 = poly2.getPolynomial();
        poly1.sortDesc(polynom1);
        poly2.sortDesc(polynom2);
        Polynomial result = new Polynomial();
        int i=0, j=0;
        while (i<polynom1.size() && j<polynom2.size()) {
            if (polynom1.get(i).getDegree() == polynom2.get(j).getDegree()) {
                float coeff = polynom1.get(i).getCoefficient() - polynom2.get(j).getCoefficient();
                if (coeff!=0)
                    result.insert(new Monomial(coeff,polynom1.get(i).getDegree()));
                i++;
                j++;
            }else if (polynom1.get(i).getDegree() > polynom2.get(j).getDegree()) {
                result.insert(new Monomial(polynom1.get(i).getCoefficient(), polynom1.get(i).getDegree()));
                i++;
            } else if (polynom1.get(i).getDegree() < polynom2.get(j).getDegree()){
                result.insert(new Monomial(-polynom2.get(j).getCoefficient(), polynom2.get(j).getDegree()));
                j++;
            }
        }
        while (i < polynom1.size()) {
            result.insert(new Monomial(polynom1.get(i).getCoefficient(), polynom1.get(i).getDegree()));
            i++;
        }
        while (j < polynom2.size()) {
            Monomial term = new Monomial(0,polynom2.get(j).getDegree());
            float coeff = -polynom2.get(j).getCoefficient();
            term.setCoefficient(coeff);
            result.insert(term);
            j++;
        }
        return result;
    }
}
