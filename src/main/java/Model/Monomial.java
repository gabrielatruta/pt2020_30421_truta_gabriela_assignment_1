package Model;
public final class Monomial {

    private float coefficient;
    private int degree;

    public Monomial (float coefficient, int degree){
        this.coefficient = coefficient;
        this.degree = degree;
    }

    public float getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(float coefficient) {
        this.coefficient = coefficient;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public String toString() {
        return this.coefficient+"x^"+this.degree;
    }

}