package Model;

import java.util.List;

public class Derivative {

    public Polynomial derivate(Polynomial poly) {

        List<Monomial> poly1 = poly.getPolynomial();
        Polynomial polyDerivated = new Polynomial();
        int i = 0;

        while(i<poly1.size()) {
            Monomial term = new Monomial(0,0);

            float coeff = poly1.get(i).getCoefficient() * poly1.get(i).getDegree();
            int degree = poly1.get(i).getDegree() - 1;

            term.setCoefficient(coeff);
            term.setDegree(degree);

            polyDerivated.insert(term);
            i++;
        }
        return polyDerivated;
    }
}
