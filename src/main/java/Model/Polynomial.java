package Model;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public final class Polynomial {

    private ArrayList<Monomial> poly = new ArrayList();

    public Polynomial (){
    }

    public List<Monomial> getPolynomial() {
        return poly;
    }

    public void insert(Monomial term) {
        this.poly.add(term);
    }

    public void sortDesc (List<Monomial> poly){
        poly.sort(Comparator.comparing(Monomial::getDegree).reversed());
    }

    public void removeMonom (Monomial term){
        List<Monomial> polynomial = this.getPolynomial();
        polynomial.remove(term);
    }

    public String toString(){
        String result = "";
        DecimalFormat decimals;

        for (Monomial term: this.poly){
            if (term.getCoefficient() == (int)term.getCoefficient())
                decimals = new DecimalFormat("0");
            else decimals = new DecimalFormat("0.00");

              if(term.getCoefficient() > 0)
                if(!result.equals(""))
                    result = result + "+";

                if(term.getCoefficient() != 1 && term.getCoefficient() != 0 && term.getCoefficient() != -1)
                    result= result + decimals.format(term.getCoefficient());
                else {

                if (term.getDegree() == 0)
                    if (term.getCoefficient() != 0)
                        result = result + decimals.format(term.getCoefficient());

                if (term.getDegree() >= 1)
                    if (term.getCoefficient() != 0)
                        if (term.getCoefficient() < 0)
                            result = result + "-";
             }if (term.getDegree()>=2)
                result=result+"x^" + term.getDegree();

              if (term.getDegree()==1)
                result=result+"x";
        }
        if (result.equals(""))
            return "0";
        return result;
    }
}
