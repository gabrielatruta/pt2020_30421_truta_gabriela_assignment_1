package View;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {

    public JTextField polynomial1Text = new JTextField();
    public JTextField polynomial2Text = new JTextField();
    public JLabel poly1Label = new JLabel("  1st Polynomial  ");
    public JLabel poly2Label = new JLabel("  2nd Polynomial  ");
    public JTextField resultText = new JTextField(30);
    public JLabel resultLabel = new JLabel("  Result");
    public JButton additionButton = new JButton("+");
    public JButton subtractionButton= new JButton("-");
    public JButton multiplicationButton = new JButton("*");
    public JButton divisionButton = new JButton("/");
    public JButton derivationButton1 = new JButton("Derive 1st");
    public JButton derivationButton2 = new JButton("Derive 2nd");
    public JButton integrationButton1 = new JButton("Integrate 1st");
    public JButton integrationButton2 = new JButton("Integrate 2nd");
    public JButton clearButton = new JButton("C");


    public GUI(){
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        panel.setPreferredSize(new Dimension(1000, 400));
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        panel.add(poly1Label, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 3;
        c.gridx = 1;
        c.gridy = 0;
        panel.add(polynomial1Text, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        panel.add(poly2Label, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx=1.5;
        c.gridwidth = 3;
        c.gridx = 1;
        c.gridy = 1;
        panel.add(polynomial2Text, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 3;
        panel.add(resultLabel, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx=1;
        c.gridy=3;
        c.gridwidth = 3;
        c.ipady = 9;
        panel.add(resultText, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 5;
        c.gridy = 0;
        c.gridwidth = 1;
        panel.add(additionButton,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 6;
        c.gridy = 0;
        c.gridwidth = 1;
        panel.add(subtractionButton,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 5;
        c.gridy = 1;
        c.gridwidth = 1;
        panel.add(multiplicationButton,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 6;
        c.gridy = 1;
        c.gridwidth = 1;
        panel.add(divisionButton,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 5;
        c.gridy = 2;
        c.gridwidth = 1;
        panel.add(integrationButton1,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 6;
        c.gridy = 2;
        c.gridwidth = 1;
        panel.add(integrationButton2,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 5;
        c.gridy = 3;
        c.gridwidth = 1;
        panel.add(derivationButton1,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 6;
        c.gridy = 3;
        c.gridwidth = 1;
        panel.add(derivationButton2,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 5;
        c.gridy = 4;
        c.gridwidth = 1;
        panel.add(clearButton,c);

        this.setContentPane(panel);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void addAdditionButtonListener(ActionListener additionLis) {
        additionButton.addActionListener(additionLis);
    }

    public void addSubtractionButtonListener(ActionListener subtractionLis) {
        subtractionButton.addActionListener(subtractionLis);
    }

    public void addMultiplicationButtonListener(ActionListener multiplicationLis) {
        multiplicationButton.addActionListener(multiplicationLis);
    }

    public void addIntegrationButton1Listener(ActionListener integration1Lis) {
        integrationButton1.addActionListener(integration1Lis);
    }

    public void addIntegrationButton2Listener(ActionListener integration2Lis) {
        integrationButton2.addActionListener(integration2Lis);
    }

    public void addDerivationButton1Listener(ActionListener derivative1Lis) {
        derivationButton1.addActionListener(derivative1Lis);
    }

    public void addDerivationButton2Listener(ActionListener derivative2Lis) {
        derivationButton2.addActionListener(derivative2Lis);
    }

    public void addClearListener(ActionListener clearLis) {
        clearButton.addActionListener(clearLis);
    }

    public JButton getAddition()
    {
        return additionButton;
    }

    public JButton getSubtraction(){
        return subtractionButton;
    }

    public JButton getMultiplication(){
        return multiplicationButton;
    }

    public JButton getDerivative1(){
        return derivationButton1;
    }

    public JButton getDerivative2(){
        return derivationButton2;
    }

    public JButton getIntegration1(){
        return integrationButton1;
    }

    public JButton getIntegration2(){
        return integrationButton2;
    }

    public JTextField getResult(){
        return resultText;
    }

}
