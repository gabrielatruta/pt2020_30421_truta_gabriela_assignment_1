package Controller;

import Model.*;
import View.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Control {
    private GUI view;

    public Control(GUI view) {
        this.view = view;

        view.addAdditionButtonListener(new AdditionButtonListener());
        view.addSubtractionButtonListener(new SubstractionButtonListener());
        view.addMultiplicationButtonListener(new MultiplicationButtonListener());
        view.addDerivationButton1Listener(new DerivationButton1Listener());
        view.addDerivationButton2Listener(new DerivationButton2Listener());
        view.addIntegrationButton1Listener(new IntegrationButton1Listener());
        view.addIntegrationButton2Listener(new IntegrationButton2Listener());
        view.addClearListener(new ClearButtonListener());

    }


    private class AdditionButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Polynomial polynomial1 = new Polynomial();
            String s1 = view.polynomial1Text.getText();
            Polynomial polynomial2 = new Polynomial();
            String s2 = view.polynomial2Text.getText();
            Addition addition = new Addition();
            Parse parsePoly = new Parse();
            if (!parsePoly.parsePolynomial(s1, polynomial1)) {
                return;
            }
            if (!parsePoly.parsePolynomial(s2, polynomial2)) {
                return;
            }
            Polynomial result = addition.additionPoly(polynomial1, polynomial2);
            view.resultText.setText(result.toString());

        }
    }

    private class SubstractionButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Polynomial polynomial1 = new Polynomial();
            String s1 = view.polynomial1Text.getText();
            Polynomial polynomial2 = new Polynomial();
            String s2 = view.polynomial2Text.getText();
            Subtraction subtraction = new Subtraction();
            Parse parsePoly = new Parse();
            if (!parsePoly.parsePolynomial(s1, polynomial1)) {
                return;
            }
            if (!parsePoly.parsePolynomial(s2, polynomial2)) {
                return;
            }
            Polynomial result = subtraction.subtractionPoly(polynomial1, polynomial2);
            view.resultText.setText(result.toString());
        }
    }

    private class MultiplicationButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Polynomial polynomial1 = new Polynomial();
            String s1 = view.polynomial1Text.getText();
            Polynomial polynomial2 = new Polynomial();
            String s2 = view.polynomial2Text.getText();
            Multiplication multiplication = new Multiplication();
            Parse parsePoly = new Parse();
            if (!parsePoly.parsePolynomial(s1, polynomial1)) {
                return;
            }
            if (!parsePoly.parsePolynomial(s2, polynomial2)) {
                return;
            }
            Polynomial result = multiplication.multiply(polynomial1, polynomial2);
            view.resultText.setText(result.toString());
        }

    }

    private class DerivationButton1Listener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Polynomial polynomial1 = new Polynomial();
            String s1 = view.polynomial1Text.getText();
            Polynomial polynomial2 = new Polynomial();
            String s2 = view.polynomial2Text.getText();
            Derivative derivative = new Derivative();
            Parse parsePoly = new Parse();
            if (!parsePoly.parsePolynomial(s1, polynomial1)) {
                return;
            }
            if (!parsePoly.parsePolynomial(s2, polynomial2)) {
                return;
            }
            Polynomial result = derivative.derivate(polynomial1);
            view.resultText.setText(result.toString());
        }
    }

    private class DerivationButton2Listener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Polynomial polynomial1 = new Polynomial();
            String s1 = view.polynomial1Text.getText();
            Polynomial polynomial2 = new Polynomial();
            String s2 = view.polynomial2Text.getText();
            Derivative derivative = new Derivative();
            Parse parsePoly = new Parse();
            if (!parsePoly.parsePolynomial(s1, polynomial1)) {
                return;
            }
            if (!parsePoly.parsePolynomial(s2, polynomial2)) {
                return;
            }
            Polynomial result = derivative.derivate(polynomial2);
            view.resultText.setText(result.toString());
        }

    }

    private class IntegrationButton1Listener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Polynomial polynomial1 = new Polynomial();
            String s1 = view.polynomial1Text.getText();
            Polynomial polynomial2 = new Polynomial();
            String s2 = view.polynomial2Text.getText();
            Integration integrate = new Integration();
            Parse parsePoly = new Parse();
            if (!parsePoly.parsePolynomial(s1, polynomial1)) {
                return;
            }
            if (!parsePoly.parsePolynomial(s2, polynomial2)) {
                return;
            }
            Polynomial result = integrate.integrate(polynomial1);
            view.resultText.setText(result.toString());
        }
    }

    private class IntegrationButton2Listener implements ActionListener {


        public void actionPerformed(ActionEvent e) {
            Polynomial polynomial1 = new Polynomial();
            String s1 = view.polynomial1Text.getText();
            Polynomial polynomial2 = new Polynomial();
            String s2 = view.polynomial2Text.getText();
            Integration integrate = new Integration();
            Parse parsePoly = new Parse();
            if (!parsePoly.parsePolynomial(s1, polynomial1)) {
                return;
            }
            if (!parsePoly.parsePolynomial(s2, polynomial2)) {
                return;
            }
            Polynomial result = integrate.integrate(polynomial2);
            view.resultText.setText(result.toString());
        }
    }

    private class ClearButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            view.polynomial1Text.setText("");
            view.polynomial2Text.setText("");
            view.resultText.setText("");
        }
    }
}

