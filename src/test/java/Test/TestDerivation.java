package Test;
import Controller.Control;
import View.GUI;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDerivation {
    @Test
    public void test1(){ //the polynomial has monoms of 0 degree
        GUI view = new GUI();
        Control control = new Control(view);
        view.polynomial1Text.setText("10+30+12+43");
        view.polynomial2Text.setText("x");
        view.getDerivative1().doClick();
        assertEquals("0", view.getResult().getText());
    }

    @Test
    public void test2(){ //the polynomial has monoms with different degrees
        GUI view = new GUI();
        Control control = new Control(view);
        view.polynomial2Text.setText("5x^3-2x^2+7");
        view.polynomial1Text.setText("x");
        view.getDerivative2().doClick();
        assertEquals("15x^2-4x", view.getResult().getText());
    }
}
