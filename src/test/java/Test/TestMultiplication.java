package Test;

import Controller.Control;
import View.GUI;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMultiplication {
    @Test
    public void test1(){ //the 2 polynomials are totally different
        GUI view = new GUI();
        Control control = new Control(view);
        view.polynomial1Text.setText("x^2+3");
        view.polynomial2Text.setText("x^3+x");
        view.getMultiplication().doClick();
        assertEquals("x^5+4x^3+3x", view.getResult().getText());
    }

    @Test
    public void test2(){ //the 2 polynomials have all the monoms in common
        GUI view = new GUI();
        Control control = new Control(view);
        view.polynomial1Text.setText("5x^3-2x^2+7");
        view.polynomial2Text.setText("2x^3-3x^2-4");
        view.getMultiplication().doClick();
        assertEquals("10x^6-19x^5+6x^4-6x^3-13x^2-28", view.getResult().getText());
    }

    @Test
    public void test3(){ // the 2 polynomials have some monoms in common
        GUI view = new GUI();
        Control control = new Control(view);
        view.polynomial1Text.setText("x+1");
        view.polynomial2Text.setText("10x^10-5x^5-5");
        view.getMultiplication().doClick();
        assertEquals("10x^11+10x^10-5x^6-5x^5-5x-5", view.getResult().getText());
    }
}
