package Test;

import Controller.Control;
import View.GUI;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestIntegration {

    @Test
    public void test1(){ //the polynomial has monoms of 0 degree
        GUI view = new GUI();
        Control control = new Control(view);
        view.polynomial1Text.setText("43");
        view.polynomial2Text.setText("x");
        view.getIntegration1().doClick();
        assertEquals("43x", view.getResult().getText());
    }

    @Test
    public void test2(){ //the polynomial has monoms with different degrees
        GUI view = new GUI();
        Control control = new Control(view);
        view.polynomial2Text.setText("5x^3-2x^2+7");
        view.polynomial1Text.setText("x");
        view.getIntegration2().doClick();
        assertEquals("1.25x^4-0.67x^3+7x", view.getResult().getText());
    }
}
